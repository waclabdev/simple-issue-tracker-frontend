import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import IssuesWrapper from './components/IssuesWrapper';

class App extends Component {
    render() {
        return (
            <div>
                <h1>Simple issue tracker</h1>
                <IssuesWrapper />
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('container'));
