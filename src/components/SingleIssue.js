import React, { Component } from 'react';
import axios from 'axios';
import moment from 'moment';

import Icon from 'react-icons-kit';
import { remove } from 'react-icons-kit/fa/remove';
import { edit } from 'react-icons-kit/fa/edit';
import config from '../../config/config';
import Status from './Status';

const root = config.root;

class SingleIssue extends Component {

    constructor(props) {
        super(props);
        this.state = {
            edit: this.props.edit,
            newTitle: this.props.issue.title,
            newContent: this.props.issue.content
        };
        this.changeStatus = this.changeStatus.bind(this);
    }

    editIssue() {
        this.props.clearEditIssue(this.props.issue._id, this.props.edit);
    }

    rewriteIssueTitle(e) {
        this.setState({newTitle: e.target.value});
    }

    rewriteIssueContent(e) {
        this.setState({newContent: e.target.value});
    }

    updateIssue() {
        let issue = Object.assign({}, this.props.issue);
        issue.title = this.state.newTitle;
        issue.content = this.state.newContent;
        issue.date = moment().format('DD/MM/YYYY');

        axios.put(root + '/' + this.props.issue._id, issue)
          .then(() => {
              this.props.clearEditIssue('', '');
              this.props.getList();
          })
          .catch((e) => console.log(e));
    }

    removeIssue() {
        axios.delete(root + '/' + this.props.issue._id)
            .then(() => {
                this.props.getList();
            })
          .catch((e) => console.log(e));
    }

    changeStatus(e) {
        if (this.props.issue.status !== 'open') {
            if (e.target.value === 'open' || this.props.issue.status === 'closed') {
                window.alert('not allowed');
                return;
            }
        }

        let issue = Object.assign({}, this.props.issue);
        issue.status = e.target.value;

        axios.put(root + '/' + issue._id, issue)
              .then(() => {
                  this.props.getList();
              })
              .catch((e) => console.log(e));
    }

    render() {
        return (
          <li className={'single-issue ' + (this.props.issue.status === 'pending' ? 'pending' : '') + (this.props.issue.status === 'closed' ? 'closed' : '')}>

              <div className="issue-panel">
                  <div className="issue-date">{this.props.issue.date}</div>
                  <div className="issue-btns">
                      <button className="issue-btn edit-issue" onClick={() => this.editIssue()}><Icon size={20} icon={edit} /></button>
                      <button className="issue-btn delete-issue" onClick={() => { if (window.confirm('Delete item?')) this.removeIssue(); }}><Icon size={20} icon={remove} /></button>
                  </div>
              </div>
              { !this.props.edit &&
                  <div>
                    <h2 className="issue-text">{this.props.issue.title}</h2>
                    <span className="issue-text">{this.props.issue.content}</span>
                  </div>
              }
              { this.props.edit &&
                  <div className="edit-issue-container">
                      <input type="text" value={this.state.newTitle} onChange={(e) => this.rewriteIssueTitle(e)}/>
                      <textarea className="issue-textarea" value={this.state.newContent} onChange={(e) => this.rewriteIssueContent(e)}> </textarea><br/>
                      <button className="btn btn-update-issue" onClick={() => this.updateIssue()}>update</button>
                  </div>
              }

              { this.props.issue.status !== 'closed' &&
                  <Status issueStatus={this.props.issue.status} changeStatus={this.changeStatus}/>
              }

              <div className="bottom-hover-strip"></div>

          </li>
        );
    }
}

SingleIssue.defaultProps = {
    edit: false
};

export default SingleIssue;
