import React, { Component } from 'react';
import SingleIssue from './SingleIssue';

class Issues extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentlyEdit: '',
            currentlyEditState: ''
        };
        this.clearEditIssue = this.clearEditIssue.bind(this);
    }

    clearEditIssue(id, editedState) {
        this.setState({
            currentlyEdit: id,
            currentlyEditState: editedState
        });
    }

    render() {
        const issues = this.props.issuesList.map((e) => {
            if (this.state.currentlyEdit === e._id) {
                if (this.state.currentlyEditState) {
                    return (<SingleIssue key={e._id} issue={e} getList={this.props.getList} clearEditIssue={this.clearEditIssue} edit={false}/>);
                } else {
                    return (<SingleIssue key={e._id} issue={e} getList={this.props.getList} clearEditIssue={this.clearEditIssue} edit={true}/>);
                }
            } else {
                return (<SingleIssue key={e._id} issue={e} title={e.title} getList={this.props.getList} clearEditIssue={this.clearEditIssue} edit={false}/>);
            }
        });

        return (
            <div className="issues">
                <h1 className="issues-header">Issues</h1>
                <ul className="issues-list">
                    {issues}
                    {issues.length === 0 &&
                        'No issues'
                    }
                </ul>
            </div>
        );
    }
}

export default Issues;
