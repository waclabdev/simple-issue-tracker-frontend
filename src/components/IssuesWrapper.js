import React, {Component} from 'react';
import AddIssue from './AddIssue';
import Issues from './Issues';
import axios from 'axios';
import config from '../../config/config';

const root = config.root;

class IssuesWrapper extends Component {

    constructor(props) {
        super(props);
        this.state = {
            issuesList: []
        };

        this.addIssue = this.addIssue.bind(this);
        this.getList = this.getList.bind(this);
    }

    componentDidMount() {
        this.getList();
    }

    getList() {
        axios.get(root)
              .then((issuesList) => {
                  this.setState({issuesList: issuesList.data});
              })
              .catch((e) => {
                  console.log(e);
              });
    }

    addIssue(singleIssue) {
        axios.post(root, singleIssue)
            .then(() => {
                this.getList();
            })
            .catch((e) => {
                console.log(e);
            });
    }

    render() {
        return (
            <div className="issues-wrapper">
                <div className="app-intro">
                    <AddIssue addIssue={this.addIssue} />
                    <Issues issuesList={this.state.issuesList} getList={this.getList}/>
                </div>
            </div>
        );
    }
}

export default IssuesWrapper;
