import React, { Component } from 'react';
import moment from 'moment';

class AddIssue extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            content: '',
            date: moment().format('DD/MM/YYYY')
        };
        this.addIssue = this.addIssue.bind(this);
    }

    addIssue() {
        if (this.state.content.length >= 5 && this.state.title.length >= 5) {
            let singleIssue = {
                title: this.state.title,
                content: this.state.content,
                date: this.state.date,
                status: 'open'
            };

            this.props.addIssue(singleIssue);

            this.setState({content: '', title: ''});
        } else {
            window.alert('You need to type at least 5 characters.');
        }
    }

    addIssueEnter(e) {
        if (e.charCode === 13 && !e.shiftKey) {
            e.preventDefault();
            this.addIssue();
        }
    }

    writeIssueTitle(e) {
        var val = e.target.value;

        if (val.length <= 50) {
            this.setState({title: val});
        } else {
            window.alert(`You can't type more than 50 characters.`);
        }
    }

    writeIssueContent(e) {
        var val = e.target.value;

        if (val.length <= 200) {
            this.setState({content: val});
        } else {
            window.alert(`You can't type more than 200 characters.`);
        }
    }

    render() {
        return (
            <div className="add-issue">
                <div className="add-issue-form-container">
                    <div action="" className="form">
                        <input className="field issue-title" onChange={(e) => this.writeIssueTitle(e)} type="text" value={this.state.title} placeholder="Issue title"/>
                        <textarea id="txt-new-issue" onChange={(e) => this.writeIssueContent(e)} onKeyPress={(e) => { this.addIssueEnter(e); }} value={this.state.content} type="text" className="field" placeholder="Issue content..." />
                        <button className="add-issue-btn" onClick={this.addIssue}>Add issue</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddIssue;
