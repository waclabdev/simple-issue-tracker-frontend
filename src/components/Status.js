import React, { Component } from 'react';

class Status extends Component {

    render() {
        return (
          <div className="status-container">
            <select className="status" name="status" id="status" value={this.props.issueStatus} onChange={this.props.changeStatus}>
                <option value="open">open</option>
                <option value="pending">pending</option>
                <option value="closed">closed</option>
            </select>
          </div>
        );
    }
}

export default Status;
