const assert = require('assert');
const expect  = require('chai').expect;
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const moment = require('moment');
const request = require('request');
const config = require('../config/config');

chai.use(chaiHttp);

const url = config.tests.url;
const base = config.tests.base;

let newIssueId = '';

const issue = {
    title: "Sample title",
    content: "Lorem ipsum",
    status: 'closed',
    date: moment().format('DD/MM/YYYY')
};

describe('Test results', () => {
    /*
     * Test the /GET route
     */
    describe('#get issues list', () => {

        it("it should GET issues list", (done) => {
            request(base+url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

    });

    /*
     * Test the /POST route
     */
    describe('#post new issue', () => {
        it('it should POST an issue', (done) => {
            chai.request(base)
                .post(url)
                .send(issue)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    newIssueId = res.body._id;
                    done();
                });
        });

    });

    /*
     * Test the /PUT route
     */
    describe('#update created issue', () => {
        it('it should UPDATE an issue', (done) => {
            issue.title = 'Changed title';
            chai.request(base)
                .put(url + '/' + newIssueId)
                .send(issue)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });

    });

    /*
     * Test the /DELETE route
     */
    describe('#delete created issue', () => {
        it('it should DELETE an issue', (done) => {
            chai.request(base)
                .delete(url + '/' + newIssueId)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });

    });

});
